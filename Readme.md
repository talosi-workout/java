# Java training

Project created by https://start.spring.io/

import : 
* Spring Web 
* Spring Data JPA 


Application propose to play at a Roleplay.

It can add several player, and organize a fight between players

## Run the application locally

Run the database
```
docker compose up
```

Environment variable to run the application
```
DATABASE_HOST=localhost;DATABASE_NAME=training-spring;DATABASE_PORT=5423;DATABASE_PWD=training-spring;DATABASE_USER=training-spring;PORT=8079
```

Application available on http://localhost:8079
health check available on : http://localhost:8079/actuator/health

swagger available on http://localhost:8079/v2/api-docs
You can use the swagger ui on http://localhost:9098 with

```bash
docker run -p 9098:8080 -e URLS="[ { url: \"http://localhost:8079/v2/api-docs\", name: \"tp-nodejs\" } ]" swaggerapi/swagger-ui
```

## api of this application

* GET /players : display all players
* PUT /players : add a player
* PATCH /players/:id : partial update a player
* DELETE /players/:id : delete a player
* POST /fight : organize a fight between players


## Some curl to test the application

```bash
curl --request GET \
  --url http://localhost:8079/players \
  --header 'Authorization: ezf'

curl --request PUT \
  --url http://localhost:8079/players \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "sangoku",
	"firstName": "San",
	"lastName" : "Go Ku",
	"sex": "MALE",
	"power": 100
}'

curl --request PUT \
  --url http://localhost:8079/players \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"username": "sankukai",
	"firstName": "San",
	"lastName" : "ku kai",
	"sex": "MALE",
	"power": 80
}'

curl --request PATCH \
  --url http://localhost:8079/players/948c2f80-0384-4871-b04b-aa62e383af64 \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"firstName": "Sandrine",
	"sex": "FEMALLE"
}'


curl --request POST \
  --url http://localhost:8079/figths \
  --header 'Authorization: ezf' \
  --header 'Content-Type: application/json' \
  --data '{
	"teams" : [
		{
			"name" : "A",
			"players" : [
				{"username":"sangoku"}
			]
		},
			{
			"name" : "B",
			"players" : [
				{"username":"sankukai"}
			]
		}
	]
}'

```
