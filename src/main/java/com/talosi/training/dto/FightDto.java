package com.talosi.training.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FightDto implements Serializable {
    @ApiModelProperty(value = "List of teams")
    private List<TeamDto> teams;

    @ApiModelProperty(value = "Final winner of the fight")
    private String winner;
}
