package com.talosi.training.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PlayerDto implements Serializable {
    @ApiModelProperty(value = "Player id")
    private String id;
    @ApiModelProperty(value = "Player username, it must be unique")
    private String username;

    @ApiModelProperty(value = "Player fisrtname")
    private String firstName;
    @ApiModelProperty(value = "Player lastname")
    private String lastName;

    @ApiModelProperty(value = "Player Sex (MALE or FEMALE)")
    private SexEnum sex;

    @ApiModelProperty(value = "Power of the player, it must be between 1 and 100")
    private int power;
}
