package com.talosi.training.controller;

import com.talosi.training.dto.health.HealthDto;
import com.talosi.training.dto.health.HealthServeurDto;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
public class HealthController {

    @ApiOperation(value = "Health check")
    @GetMapping
    public HealthDto health() {
        return HealthDto.builder()
                .server(
                        HealthServeurDto.builder()
                            .status("UP")
                            .build())
                .build();
    }


}
